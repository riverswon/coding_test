import sys
import numpy as np

def main(lines):
	n = len(lines) - 2
	num = []
	moji = []
	m = int(lines[n])
	for i in range(n):
		a,b = lines[i].split(':')
		num.append(int(a))
		moji.append(b)
	# sort by number
	NUM = np.array(num)
	MOJI = np.array(moji)
	moji = list(MOJI[NUM.argsort()])
	num.sort()

	# 数値検証
	mojiretsu = []
	for i in range(n):
	  	if m%num[i] == 0:
			  mojiretsu.append(moji[i])
	
	if mojiretsu == []:
	  	result = m
	
	else:
		result = ' '
		for i in mojiretsu:
			result += i
		result = result[1:]

	
	print(result)

if __name__ == '__main__':
	f = open('input.txt')
	lines = []
	line = f.readline()
	line = line[:len(line)-1]
	lines.append(line)
	while line:
		line = f.readline()
		line = line[:len(line)-1]
		lines.append(line)
	main(lines)
